// Initialisation de l'éditeur CodeMirror
var codeTextarea = document.getElementById('markdown-input');
var editor = CodeMirror.fromTextArea(codeTextarea, {
  mode: 'text/x-markdown',
  theme: 'cobalt',
  lineNumbers: true,
  lineWrapping: true
});

// Initialisation de Markdown-it avec des options spécifiques
var md = window.markdownit({
  breaks: true,
  typographer: true
}).use(window.markdownitEmoji);

// Met à jour l'aperçu à chaque changement dans l'éditeur
editor.on('change', function() {
  updatePreview();
});

// Fonction pour traiter les expressions LaTeX dans le texte
function processLatex(inputText) {
  // Remplace le texte entre "$$" par "\\[ ... \\]" pour les équations centrées
  inputText = inputText.replace(/\$\$(.*?)\$\$/g, function(match, equation) {
    equation = equation.replace(/%/g, '&#92;\%');
    equation = equation.replace(/€/g, '&#92;textrm{€}');
    equation = equation.replace(/(?<!\\),/g, '{,}');
    return '&#92;[' + equation + '&#92;]';
  });

  // Remplace le texte entre "$" par "\\( ... \\)" pour les équations en ligne
  inputText = inputText.replace(/\$(.*?)\$/g, function(match, equation) {
    equation = equation.replace(/%/g, '&#92;\%');
    equation = equation.replace(/€/g, '&#92;textrm{€}');
    equation = equation.replace(/,/g, '{,}');
    return '&#92;\(' + equation + '&#92;\)';
  });

  return inputText;
}

// Fonction pour échapper les caractères '<' et '>' dans les blocs de code
function escapeCodeBlocks(markdownText) {
  // Pour les blocs de code avec triple backticks
  markdownText = markdownText.replace(/```([\s\S]*?)```/g, function(match, code) {
    code = code.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return '```' + code + '```';
  });

  // Pour le code en ligne avec un seul backtick
  markdownText = markdownText.replace(/`([^`]*?)`/g, function(match, code) {
    code = code.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return '`' + code + '`';
  });

  return markdownText;
}

// Fonction pour mettre à jour l'aperçu en temps réel
function updatePreview() {
  var inputText = editor.getValue();
  inputText = inputText.replace(/"/g, '&quot;');

  // Gestion des blocs dépliables
  let lines = inputText.split("\n");
  let output = "";
  let inDetailsBlock = false;

  lines.forEach((line) => {
    if (line.startsWith(">:::")) {
      inDetailsBlock = true;
      output += `<details><summary>${line.slice(5)}</summary>\n`;
    } else if (line.startsWith(">") && inDetailsBlock) {
      output += `${line.slice(1)}\n`;
    } else if (inDetailsBlock) {
      inDetailsBlock = false;
      output += "</details>\n";
    } else {
      output += `${line}\n`;
    }
  });

  if (inDetailsBlock) {
    output += "</details>\n";
  }

  inputText = output;

  // Séparer les blocs de code du reste du texte
  let segments = inputText.split(/(```[\s\S]*?```)/g);

  // Appliquer la transformation sur les segments non-code
  for (let i = 0; i < segments.length; i++) {
    if (!segments[i].startsWith("```")) {
      segments[i] = processLatex(segments[i]);
      segments[i] = escapeCodeBlocks(segments[i]);
    }
  }

  inputText = segments.join("");

  var html = md.render(inputText);
  var decodedHtml = he.decode(html);

  // Supprimer les <br> à l'intérieur des balises <script>
  decodedHtml = decodedHtml.replace(/<script[^>]*>([\s\S]*?)<\/script>/g, function(match, content) {
    return match.replace(/<br>/g, '');
  });

  document.getElementById("rendered-content").innerHTML = decodedHtml;
  MathJax.typesetClear([document.getElementById("rendered-content")]);
  MathJax.typesetPromise([document.getElementById("rendered-content")]).then(() => {
    MathJax.typeset([document.getElementById("rendered-content")]);
  });

  hljs.highlightAll();
}

// Fonction pour mettre à jour l'aperçu pour le téléchargement
function updatePreview2() {
  var inputText = editor.getValue();
  inputText = inputText.replace(/"/g, '&quot;');

  // Gestion des blocs dépliables
  let lines = inputText.split("\n");
  let output = "";
  let inDetailsBlock = false;

  lines.forEach((line) => {
    if (line.startsWith(">:::")) {
      inDetailsBlock = true;
      output += `<details><summary>${line.slice(5)}</summary>\n`;
    } else if (line.startsWith(">") && inDetailsBlock) {
      output += `${line.slice(1)}\n`;
    } else if (inDetailsBlock) {
      inDetailsBlock = false;
      output += "</details>\n";
    } else {
      output += `${line}\n`;
    }
  });

  if (inDetailsBlock) {
    output += "</details>\n";
  }

  inputText = output;

  // Séparer les blocs de code du reste du texte
  let segments = inputText.split(/(```[\s\S]*?```)/g);

  // Appliquer la transformation sur les segments non-code
  for (let i = 0; i < segments.length; i++) {
    if (!segments[i].startsWith("```")) {
      segments[i] = processLatex(segments[i]);
      segments[i] = escapeCodeBlocks(segments[i]);
    }
  }

  inputText = segments.join("");

  var html = md.render(inputText);
  var decodedHtml = he.decode(html);

  // Supprimer les <br> à l'intérieur des balises <script>
  decodedHtml = decodedHtml.replace(/<script[^>]*>([\s\S]*?)<\/script>/g, function(match, content) {
    return match.replace(/<br>/g, '');
  });

  document.getElementById("rendered-download").innerHTML = decodedHtml;
}

// Fonction pour télécharger l'aperçu en tant que fichier HTML
function downloadPreview() {
  updatePreview2();
  // Récupérer le contenu de la balise h1
  var pageTitle = document.querySelector("h1").textContent;

  // Mettre à jour le titre de la page
  document.title = pageTitle;

  // Construire le contenu HTML avec les styles et scripts nécessaires
  var htmlContent = "<html><head><meta charset='utf-8'>" 
    + document.getElementById("ForPhone").outerHTML 
    + "<title>" + pageTitle + "</title>" 
    + document.getElementById("mathjax-id").outerHTML
    + document.getElementById("style-tikzjax").outerHTML
    + document.getElementById("js-tikzjax").outerHTML 
    + document.getElementById("font-awesome-id").outerHTML 
    + document.getElementById("style-id").outerHTML 
    + document.getElementById("highlight-id").outerHTML
    + document.getElementById("highlight-id2").outerHTML
    // Ajout du script de surlignage
    + document.getElementById("highlight-script-id").outerHTML
    + "</head><body>" + document.getElementById("rendered-download").innerHTML 
    + "</body></html>";
  
  // Supprimer les <br> à l'intérieur des balises <script>
  htmlContent = htmlContent.replace(/<script[^>]*>([\s\S]*?)<\/script>/g, function(match, content) {
    return match.replace(/<br>/g, '');
  });
  
  // Créer un objet Blob et enregistrer le fichier
  var blob = new Blob([htmlContent], {type: "text/html;charset=utf-8"});
  saveAs(blob, document.getElementById("htmlFilename").value + ".html");
}

// Fonction pour télécharger le contenu en Markdown
function downloadMarkdown() {
  var markdownContent = editor.getValue();
  var blob = new Blob([markdownContent], {type: "text/markdown;charset=utf-8"});
  saveAs(blob, document.getElementById("markdownFilename").value + ".md");
}

// Fonction pour ajouter des balises Markdown autour du texte sélectionné
function addMarkdown(startTag, endTag='') {
  var doc = editor.getDoc();
  var cursor = doc.getCursor();
  var selection = doc.getSelection();

  if (!selection) {
    // Si aucun texte n'est sélectionné, ajoute les balises au curseur
    doc.replaceRange(startTag + endTag, cursor);
    doc.setCursor(cursor.line, cursor.ch + startTag.length);
  } else {
    // Si du texte est sélectionné, encadre-le avec les balises
    doc.replaceSelection(startTag + selection + endTag);
    // Replace le curseur correctement
    if (endTag) {
      cursor.ch += startTag.length;
      doc.setCursor(cursor);
    }
  }

  updatePreview(); // Met à jour l'aperçu après la mise en forme
}

// Met à jour l'aperçu lorsque le document est chargé
document.addEventListener('DOMContentLoaded', function() {
  updatePreview();
});

// Fonction pour synchroniser le défilement entre l'éditeur et l'aperçu
function syncScroll(sourceElement, targetElement) {
  var sourceScrollRatio = sourceElement.scrollTop / (sourceElement.scrollHeight - sourceElement.clientHeight);
  var targetScrollRatio = targetElement.scrollTop / (targetElement.scrollHeight - targetElement.clientHeight);

  // Calculer la différence en pourcentage entre les positions de défilement
  var diff = Math.abs(sourceScrollRatio - targetScrollRatio);

  // Synchroniser seulement si la différence dépasse le seuil (0,5% dans cet exemple)
  if (diff > 0.005) {
    targetElement.scrollTop = (targetElement.scrollHeight - targetElement.clientHeight) * sourceScrollRatio;
  }
}

// Sélecteurs pour l'éditeur et l'aperçu
var editorElement = document.querySelector('.CodeMirror-scroll'); // Éditeur
var previewElement = document.getElementById('rendered-content'); // Aperçu

// Événements pour synchroniser le défilement
editorElement.addEventListener('scroll', function() {
  syncScroll(editorElement, previewElement);
});

previewElement.addEventListener('scroll', function() {
  syncScroll(previewElement, editorElement);
});
