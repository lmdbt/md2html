# <img src="https://ciaconelli.forge.aeif.fr/md2html/MD2HTML-logoSVG.svg" style="box-shadow: none;">

## MD2HTML, c'est quoi ?

MD2HTML est un convertisseur en ligne qui vous permet de convertir facilement votre code Markdown en HTML. Il dispose de fonctionnalités avancées telles que la coloration syntaxique, la prise en charge des équations MathJax, la possibilité d'ajouter des favicons et la capacité d'ajouter du code HTML. Vous pouvez exporter votre fichier en HTML ou en texte pour une intégration facile à votre site web.

## Technologies utilisées

MD2HTML utilise les technologies suivantes :

- [Markdown-it](https://github.com/markdown-it/markdown-it) pour la conversion de Markdown en HTML
- [MathJax](https://github.com/mathjax/MathJax) pour l'affichage des équations mathématiques
- [FileSaver.js](https://github.com/eligrey/FileSaver.js/) pour l'exportation de fichiers en HTML ou en texte
- [he.js](https://github.com/mathiasbynens/he) pour l'encodage et le décodage des caractères HTML
- [Font Awesome](https://fontawesome.com/) pour les icônes
- [markdown-it-emoji](https://cdnjs.cloudflare.com/ajax/libs/markdown-it-emoji/2.0.2/markdown-it-emoji.min.js) pour l'affichage des emojis en Markdown


## Fonctionnalités

- Conversion d'un code Markdown en HTML avec coloration syntaxique
- Exportation de fichiers au format HTML (.html) et au format en texte (.md)
- Prise en charge des équations mathématiques écrite en $\LaTeX$ à l'aide de MathJax
- Possibilité d'ajouter du code HTML
- Prise en charge des favicons à l'aide de Font Awesome
- Affichage adapté pour téléphone
- Coloration syntaxique de certains des languages $\LaTeX$, `python`, `css`, `JavaScript`, `Matlab` et `r` dans la page HTML téléchargée
- Un thème sombre est disponible
- Ajout de zones de texte dépliables et repliables

## Styles CSS personnalisés

MD2HTML utilise des styles CSS personnalisés pour améliorer la présentation du HTML généré à partir de votre Markdown. Dans la version 1.12, un style CSS dédié aux balises `<details>` et `<summary>` a été ajouté pour améliorer l'expérience utilisateur.

## Les versions

### MD2HTML version 1.16
- Ajout de pages en [anglais](https://ciaconelli.forge.aeif.fr/md2html/index.eng.html), [allemand](https://ciaconelli.forge.aeif.fr/md2html/index.de.html) et [espagnol](https://ciaconelli.forge.aeif.fr/md2html/index.sp.html).

### MD2HTML version 1.15
- Ajout d'une page entièrement en italien : [index.it.html](https://ciaconelli.forge.aeif.fr/md2html/index.it.html). Merci à [@nilocram](https://piaille.fr/@nilocram@framapiaf.org) pour la traduction.

### MD2HTML version 1.14

- Ajout d'une fonction JS pour synchroniser automatiquement le `code` et l'aperçu `html`

### MD2HTML version 1.13

- Ajout de puces/boutons pour mettre en page le document et rendre plus accessible l'outil.

>::: MD2HTML version 1.12
>- Ajout de la fonctionnalité de zones de texte dépliables et repliables. Cette fonction permet de replier du texte ou du code sous une balise `<summary>`, rendant le contenu plus organisé et facile à naviguer.
>- Ajout d'un style CSS dédié pour les balises `<details>` et `<summary>` pour améliorer leur apparence.

### MD2HTML version 1.11
- Ajout de la coloration syntaxique dans la page html téléchargée à l'aide de la librairie : `highlight.js` pour les languages $\LaTeX$, `python`, `css`, `JavaScript`, `Matlab` et `r`.

### MD2HTML version 1.10.2
- Légère modification du style CSS. Ajout d'un style `html` et `margin` à 0 pour le `body`.


### MD2HTML version 1.10.1
- Modification de la librairie Mathjax `tex-mml-svg.min.js` pour `tex-svg-full.js` notamment pour la prise en charge de l'écriture d'équation chimique.

### MD2HTML version 1.9.2
- Ajout de quelques lignes pour permettre le téléchargement de la page en HTML sans titre si aucun titre en `<h1>` n'est donné par l'utilisateur.
- Ajout de liens d'ancrage dans la table des matières du code de la page.

### MD2HTML version 1.9.1
- Toutes les librairies sont chargées depuis la forge pour ne pas (plus) dépendre d'un tier serveur.

### MD2HTML version 1.9
- Intégration de la coloration syntaxique du code à l’aide de `CodeMirror`.

### MD2HTML version 1.8.1
- Correction de "bug" : possibilité d'écrire du code HTML au format code sans qu'il soit analysé.
- Suppression de l'espace décimal en `$mode maths$` : en `$mode maths$` la virgule `,` est remplacée par `{,}` pour supprimer systematiquement l'espace décimal après la virgule.

### MD2HTML version 1.8
- Modification des fonctions "preview" (qui transposent le code Markdown en HTML) pour pouvoir écrire directement les symbols `€` et `%` en mode maths (comme cela est possible dans CodiMD).

### MD2HTML version 1.7.2
- Ajout d'un style pour l'insertion de vidéos en iframe afin qu'elles prennent 100% de la largeur en gardant un ratio 16/9.

### MD2HTML version 1.7.1
- Légère modification du style CSS → Largeur 900px pour la style de la page téléchargé.
- Légère modification du body de la page → Max-width 1500px et width 95% pour le body la page MD2HTML.

### MD2HTML version 1.7
- La page s'adapte au téléphone : 
    - Ajout d'un `media query` dans le style CSS
    - Ajout d'une balise `<meta>` dédiée dans le préambule de la page HTML

### MD2HTML version 1.6
- Le style CSS de la page est maintenant chargé dans une balise <link>
- Le fond de la page MD2HTML est grisé pour mieux distingué la fenêtre de code et la fenêtre d'aperçu

### MD2HTML version 1.5.1
- Ajout de la prise en charge des émoticones comme `:smile:`.

### MD2HTML version 1.5
- Possibilité de taper les équations en ligne directement entre `$` plutôt qu'entre `\\(` et `\\)`.
- Ajout d'une barre de défilement (scroll bar) dans la zone de code et dans l'aperçu.
- Petites modifications pour limiter des petits "lags"

### MD2HTML version 1.4
- Ajoute du script Mathjax.js dans le head de la page HTML téléchargée
- Les équations sont intégrées sous "format code" puis interprétées dans la page HTML script Mathjax
- Les élements du head sont appelé à l'aide de```document.getElementById``` pour faciliter leur mis à jour.
- Ajout d'un ```textarea``` au-dessus des boutons de téléchargement pour nommer les fichiers télécharger.

### MD2HTML version 1.3
- Ajout d'un ```<link>``` vers un "font-awesome" dans la page HTML téléchargée (les favicons n'apparaissaient pas dans la version précédente)
- Le titre de la page est le titre principal du document
- Ajout d'un ```<meta charset="utf-8">``` dans la page HTML téléchargée pour un affichage avec des caractères accentués sur téléphone et tablette.

### MD2HTML version 1.2
- Possibilité de coder en HTML
- Possibilité d'insérer des favicon
NB : Abandon de la version déconnectée pour l'instant. Mais elle reste facile à réaliser à partir de cette version.

### MD2HTML version 1.1
- Possibilité d'enregistrer son code Markdown
- Ajout d'un thème sombre

### MD2HTML version 0.3
- Transforme du code Markdown en page HTML
- Besoin d'un accès à internet pour charger les bibliothèques JS nécessaires

### MD2HTML_0.4
- Transforme du code Markdown en page HTML
- Les bibliothèques JS sont présentes dans l'archive. Cette version est donc utilisable déconnectée.

## Crédits

- markdown-it - Bibliothèque JavaScript pour l'analyse et la mise en forme de texte en markdown. Version 13.0.1. Disponible sur https://cdnjs.cloudflare.com/ajax/libs/markdown-it/13.0.1/markdown-it.js.
- mathjax - Bibliothèque JavaScript pour afficher des formules mathématiques sur le web. Version 3.2.2. Disponible sur https://cdnjs.cloudflare.com/ajax/libs/mathjax/3.2.2/es5/tex-chtml-full-speech.min.js.
- FileSaver.js - Bibliothèque JavaScript pour permettre le téléchargement de fichiers côté client. Version 2.0.0. Disponible sur https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js.
- he.js - Bibliothèque JavaScript pour encoder et décoder les entités HTML. Version 1.2.0. Disponible sur https://cdnjs.cloudflare.com/ajax/libs/he/1.2.0/he.js.
- Font Awesome - Bibliothèque d'icônes en ligne. Version 6.4.0. Disponible sur https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css.
- markdown-it-emoji - Bibliothèque JavaScript pour ajouter la prise en charge des emojis à Markdown-IT. Version 2.0.2. Disponible sur https://cdnjs.cloudflare.com/ajax/libs/markdown-it-emoji/2.0.2/markdown-it-emoji.min.js.
- highlight.js - Bibliothèque JavaScript pour la mise en évidence syntaxique du code. Version 11.8.0. Disponible sur [highlightjs.org](https://highlightjs.org/).


## Exemple de pages réalisées avec MD2HTML

- [Une aide](https://www.lmdbt.fr/notes/AideDMn3.html) sur un exercice dans un devoir maison donné à des élèves.
- [Une version de ce readme](readme.html) réalisé avec MD2HTML.

## Licenses

### Licences des bibliothèques

Voici les licences des bibliothèques utilisées dans cette application :

- `markdown-it` : distribué sous la licence MIT.
- `mathjax` : distribué sous la licence Apache 2.0.
- `FileSaver.js` : distribué sous la licence MIT.
- `he.js` : distribué sous la licence MIT.
- `Font Awesome` : distribué sous la licence SIL OFL 1.1.
- `markdown-it-emoji` : distribué sous la licence MIT.
- `highlight.js` : distribué sous la licence BSD 3-Clause.


Veuillez noter que ces bibliothèques sont distribuées sous des licences open source, ce qui signifie que vous pouvez les utiliser et les redistribuer conformément aux conditions de chaque licence. Veuillez consulter les licences respectives pour plus d'informations sur les conditions de chaque licence.

### Licence du code de la page HTML

Le code de la page HTML est quant à lui en licence CC-BY.

![ccby](https://forge.aeif.fr/ciaconelli/md2html/-/raw/main/img/by.svg).

